import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_maven_version(host):
    cmd = host.run('mvn --version')
    assert 'Apache Maven 3.5.3' in cmd.stdout


def test_ant_version(host):
    cmd = host.run('ant -version')
    assert 'Apache Ant(TM) version 1.10.3' in cmd.stdout
