ics-ans-role-dev-tools
======================

Ansible role to install development tools.

It currently installs:

 - git
 - make
 - gcc
 - maven
 - ant
 - vim / emacs

It can also give permissions to run "sudo yum" to a specific user.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
dev_tools_maven_version: 3.5.3
dev_tools_maven_url: https://artifactory.esss.lu.se/artifactory/swi-pkg/apache/maven/{{ dev_tools_maven_version }}/apache-maven-{{ dev_tools_maven_version }}-bin.tar.gz
dev_tools_ant_version: 1.10.3
dev_tools_ant_url: https://artifactory.esss.lu.se/artifactory/swi-pkg/apache/ant/{{ dev_tools_ant_version }}/apache-ant-{{ dev_tools_ant_version }}-bin.zip
# Set to a valid user to allow him to run "sudo yum"
dev_tools_sudo_user: ""
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-dev-tools
```

License
-------

BSD 2-clause
